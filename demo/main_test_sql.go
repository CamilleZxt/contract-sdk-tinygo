/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0

main_fact_sql
*/

package main

import (
	"chainmaker.org/contract-sdk-tinygo/sdk/convert"
)

//安装合约时会执行此方法，必须
//export init_contract
func initContract() {
	ctx := NewSqlSimContext()
	ctx.Log("init_contract [start]")

	// create teacher
	sqlCreateTeacher := `create table teacher_gasm (
							id varchar(128) primary key,
							name varchar(64) DEFAULT ''
						)
						`
	ctx.Log(sqlCreateTeacher)
	_, resultCode := ctx.ExecuteDdl(sqlCreateTeacher)
	if resultCode != SUCCESS {
		msg := "initContract error. resultCode=" + convert.Int32ToString(int32(resultCode)) + " sqlCreateTeacher=" + sqlCreateTeacher
		ctx.Log(msg)
		ctx.ErrorResult(msg)
		return
	} else {
		ctx.Log("create table teacher_gasm success.")
	}

	// create student
	sqlCreateStudent := `create table student_gasm (
					id varchar(128) primary key,
					teacher_id varchar(128),
					name varchar(64) DEFAULT '',
					age int DEFAULT 0,
					score int DEFAULT 0,
					id_card_no varchar(19) DEFAULT '',
					attend_school_time date
					)
				`
	ctx.Log(sqlCreateStudent)
	_, resultCode = ctx.ExecuteDdl(sqlCreateStudent)
	if resultCode != SUCCESS {
		msg := "initContract error. resultCode=" + convert.Int32ToString(int32(resultCode)) + " sqlCreateStudent=" + sqlCreateStudent
		ctx.Log(msg)
		ctx.ErrorResult(msg)
		return
	} else {
		ctx.Log("create table student_gasm success.")
	}

	ctx.SuccessResult("create table student、teacher_gasm success")
	ctx.Log("initContract success.")
	//ctx.Log("init_contract [end]")

	////autoIncrementSql()
	////createDb()
	////createUser()
	//ctx.Log("init_contract [end]")
	////createStateTable()
	////autoIncrementSql()
	//if !autoIncrementSql(){
	//	ctx.Log("autoIncrementSql校验成功")
	//	ctx.Log("######################################################################################\n")
	//}
	//if !createDb(){
	//	ctx.Log("createDb校验成功")
	//	ctx.Log("######################################################################################\n")
	//}
	//if !createUser(){
	//	ctx.Log("createUser校验成功")
	//	ctx.Log("######################################################################################\n")
	//}
}

// 升级合约时会执行此方法，必须
//export upgrade
func upgrade() {
	ctx := NewSqlSimContext()
	ctx.Log("upgrade [start]")

	sqlAddColumn := "ALTER TABLE student_gasm ADD address varchar(255) NULL"
	ctx.Log(sqlAddColumn)

	_, resultCode := ctx.ExecuteDdl(sqlAddColumn)

	if resultCode != SUCCESS {
		msg := "upgrade error."
		ctx.Log(msg)
		ctx.ErrorResult(msg)
	} else {
		ctx.Log("upgrade success.")
		ctx.SuccessResult("upgrade success.")
	}
	ctx.Log("upgrade [end]")
}

//export sql_insert
func sqlInsert() {
	ctx := NewSqlSimContext()
	ctx.Log("sql_insert [start]")

	id, _ := ctx.ArgString("id")
	age, _ := ctx.ArgString("age")
	name, _ := ctx.ArgString("name")
	idCardNo, _ := ctx.ArgString("id_card_no")

	if len(id) == 0 || len(age) == 0 {
		ctx.Log("param id/age is required")
		ctx.ErrorResult("param id/age is required")
		return
	}

	// insert fmt.Sprintf("insert into student_gasm(id, name, age, id_card_no) VALUES ('%s', '%s', '%s', '%s')", id, name, age, idCardNo)
	sqlInsert := "insert into student_gasm(id, name, age, id_card_no) VALUES ('" + id + "', '" + name + "', '" + age + "', '" + idCardNo + "')"
	ctx.Log("sql_insert [sql]" + sqlInsert)

	rowCount, resultCode := ctx.ExecuteUpdate(sqlInsert)

	if resultCode != SUCCESS {
		ctx.Log("sql_insert error")
		ctx.ErrorResult("sql_insert error")
		return
	} else {
		msg := "sql_insert update row=" + convert.Int32ToString(rowCount)
		ctx.Log(msg)
	}
	ctx.SuccessResult("ok")
	ctx.Log("sql_insert [end] ok")
}

//export sql_query_by_id
func sqlQueryById() {
	ctx := NewSqlSimContext()
	ctx.Log("sql_query_by_id [start]")

	id, _ := ctx.ArgString("id")
	if len(id) == 0 {
		ctx.Log("param id is required")
		ctx.ErrorResult("param id is required")
		return
	}

	sqlQuery := "select id, name, age, id_card_no from student_gasm where id='" + id + "'"
	ctx.Log(sqlQuery)

	ec, resultCode := ctx.ExecuteQueryOne(sqlQuery)

	if resultCode != SUCCESS {
		ctx.Log("ExecuteQueryOne error")
		ctx.ErrorResult("ExecuteQueryOne error")
		return
	}
	jsonStr := ec.ToJson()
	ctx.Log("sql_query_by_id ok result:" + jsonStr)
	ctx.SuccessResult(jsonStr)
	ctx.Log("sql_query_by_id [end]")
}

//export sql_query_range_of_age
func sqlQueryRangeOfAge() {
	ctx := NewSqlSimContext()
	ctx.Log("sql_query_range_of_age [start]")

	maxAge, _ := ctx.ArgString("max_age")
	minAge, _ := ctx.ArgString("min_age")
	if len(maxAge) == 0 || len(minAge) == 0 {
		ctx.Log("param max_age/min_age is required")
		ctx.ErrorResult("param max_age/min_age is required")
		return
	}

	sqlQuery := "select id, name, age, id_card_no from student_gasm where age>" + minAge + " and age<" + maxAge
	ctx.Log(sqlQuery)

	resultSet, resultCode := ctx.ExecuteQuery(sqlQuery)
	if resultCode != SUCCESS {
		ctx.Log("ExecuteQuery error")
		ctx.ErrorResult("ExecuteQuery error")
		return
	}
	var result string
	for resultSet.HasNext() {
		resultSet.NextRow()
		ec, resultCode := resultSet.NextRow()
		if resultCode != SUCCESS {
			ctx.Log("NextRow error")
			ctx.ErrorResult("NextRow error")
			return
		}
		jsonStr := ec.ToJson()
		ctx.Log("NextRow: " + jsonStr)
		result += jsonStr
	}
	resultSet.Close()

	ctx.SuccessResult(result)
	ctx.Log("sql_query_range_of_age [end]")
}

//export sql_update
func sqlUpdate() {
	ctx := NewSqlSimContext()
	ctx.Log("sql_update [start]")

	name, _ := ctx.ArgString("name")

	// insert
	sqlInsert := "update student_gasm set name='" + name + "' "
	ctx.Log(sqlInsert)

	rowCount, resultCode := ctx.ExecuteUpdate(sqlInsert)

	if resultCode != SUCCESS {
		ctx.Log("ExecuteUpdateSingle error")
		ctx.ErrorResult("ExecuteUpdateSingle error")
		return
	} else {
		msg := "ExecuteUpdateSingle update row=" + convert.Int32ToString(rowCount)
		ctx.Log(msg)
	}
	ctx.SuccessResult("ok")
	ctx.Log("sql_update [end]")
}

//export sql_update_rollback_save_point
func sqlUpdateRollbackSavePoint() {
	ctx := NewSqlSimContext()
	ctx.Log("sql_update_rollback_save_point [start]")
	name, _ := ctx.ArgString("name")
	txId, _ := ctx.GetTxId()

	// insert
	sqlUpdate := "update student_gasm set name='" + name + "'"
	sqlInsert := "insert into student_gasm(id, name, age, id_card_no) VALUES ('" + txId + "', 'Tom', '18', '409787417841395')"
	ctx.Log(sqlInsert)
	ctx.Log(sqlUpdate)

	rowCount, resultCode := ctx.ExecuteUpdate(sqlUpdate)
	ctx.ExecuteUpdate(sqlInsert)

	if resultCode != SUCCESS {
		ctx.Log("ExecuteUpdateSingle error")
		ctx.ErrorResult("ExecuteUpdateSingle error")
		return
	} else {
		msg := "ExecuteUpdateSingle update row=" + convert.Int32ToString(rowCount)
		ctx.Log(msg)
	}
	ctx.ErrorResult("test: save point error test")
	ctx.Log("sql_update_rollback_save_point [end]")
}

//export sql_delete
func sqlDelete() {
	ctx := NewSqlSimContext()
	ctx.Log("sql_delete [start]")

	id, _ := ctx.ArgString("id")

	// insert
	sqlInsert := "delete from student_gasm where id='" + id + "'"
	ctx.Log(sqlInsert)

	rowCount, resultCode := ctx.ExecuteUpdate(sqlInsert)

	if resultCode != SUCCESS {
		ctx.Log("ExecuteUpdateSingle error")
		ctx.ErrorResult("ExecuteUpdateSingle error")
		return
	} else {
		msg := "ExecuteUpdateSingle update row=" + convert.Int32ToString(rowCount)
		ctx.Log(msg)
	}
	ctx.SuccessResult("ok")
	ctx.Log("sql_delete [end]")
}

//export sql_cross_call
func sqlCrossCall() {
	ctx := NewSqlSimContext()
	ctx.Log("sql_cross_call [start]")
	contractName, _ := ctx.ArgString("contract_name")
	minAge, _ := ctx.ArgString("min_age")
	maxAge, _ := ctx.ArgString("max_age")
	if len(contractName) == 0 {
		contractName = "contract01"
	}
	if len(minAge) == 0 {
		minAge = "0"
	}
	if len(maxAge) == 0 {
		maxAge = "20"
	}
	param := map[string][]byte{
		"min_age": []byte(minAge),
		"max_age": []byte(maxAge),
	}

	result, code := ctx.CallContract(contractName, "sql_query_range_of_age", param)
	if code != SUCCESS {
		ctx.Log("contract inner call contract fail.")
		ctx.ErrorResult("contract inner call contract fail.")
		return
	}
	ctx.Log(string(result))
	ctx.SuccessResultByte(result)
	ctx.Log("sql_cross_call [end]")
}

//测试函数handFunc(?)

//export printhello
func printhello() {
	ctx := NewSqlSimContext()
	ctx.Log("hello!")
}

////export creatTeacher
//func creatTeacher(){
//	ctx := NewSqlSimContext()
//	ctx.Log("init_contract [start]")
//	// create teacher
//	sqlCreateTeacher := `create table teacher_gasm (
//                     id varchar(128) primary key,
//                     name varchar(64) DEFAULT ''
//                  )
//                  `
//	ctx.Log(sqlCreateTeacher)
//	_, resultCode := ctx.ExecuteDdl(sqlCreateTeacher)
//	if resultCode != SUCCESS {
//		msg := "initContract error. resultCode=" + strconv.Itoa(int(resultCode)) + " sqlCreateTeacher=" + sqlCreateTeacher
//		ctx.Log(msg)
//		ctx.ErrorResult(msg)
//		return
//	} else {
//		ctx.Log("create table teacher_gasm success.")
//	}
//	ctx.Log("errortest!")
//}

//export createTable
func createTable() {
	ctx := NewSqlSimContext()
	ctx.Log("init_contract [start]")
	sqlCreatetable := `create table whang(
				id int(32) primary key,
				username varchar(50)
							)`
	ctx.Log(sqlCreatetable)
	_, resultCode := ctx.ExecuteDdl(sqlCreatetable)
	if resultCode != SUCCESS {
		msg := "initContract error. resultCode=" + convert.Int32ToString(int32(resultCode)) + " sqlIncrement=" + sqlCreatetable
		ctx.Log(msg)
		ctx.ErrorResult(msg)
		return
	} else {
		ctx.Log("creat whang success.")
	}
}

//export createStateTable
func createStateTable() { //在初始化函数中调用
	ctx := NewSqlSimContext()
	ctx.Log("init_contract [start]")
	sqlCreatetable := `create table state_infos(
				id int(32) primary key,
				username varchar(50)
							)`
	ctx.Log(sqlCreatetable)
	_, resultCode := ctx.ExecuteDdl(sqlCreatetable)
	if resultCode != SUCCESS {
		msg := "initContract error. resultCode=" + convert.Int32ToString(int32(resultCode)) + " sqlIncrement=" + sqlCreatetable
		ctx.Log(msg)
		ctx.ErrorResult(msg)
		return
	} else {
		ctx.Log("creat whang success.")
		return
	}
}

//export doubleSql
func doubleSql() {
	ctx := NewSqlSimContext()
	ctx.Log("init_contract [start]")
	// create doubleSql
	sqlDouble := "insert into student_gasm(id, name, age, id_card_no) VALUES ('34134','whang','1234','1342551153');" +
		"insert into student_gasm(id, name, age, id_card_no) VALUES ('34134','whang','1234','1342551153')"

	ctx.Log(sqlDouble)
	_, resultCode := ctx.ExecuteUpdate(sqlDouble)
	if resultCode != SUCCESS {
		msg := "initContract error. resultCode=" + convert.Int32ToString(int32(resultCode)) + " sqlDouble=" + sqlDouble
		ctx.Log(msg)
		ctx.ErrorResult(msg)
		return
	} else {
		ctx.Log("detect table1 success.")
	}
}

//export commitSql
func commitSql() {
	ctx := NewSqlSimContext()
	ctx.Log("init_contract [start]")
	// create doubleSql
	sqlInsert := "insert into student_gasm(id, name, age, id_card_no) VALUES ('34134','whang','1234','1342551153')"
	sqlComit := "COMMIT"
	ctx.Log(sqlComit)
	ctx.Log(sqlInsert)
	_, insertCode := ctx.ExecuteUpdate(sqlInsert)
	if insertCode == SUCCESS {
		ctx.Log("插入数据成功")
	}
	_, resultCode := ctx.ExecuteUpdate(sqlComit)
	if resultCode != SUCCESS {
		msg := "initContract error. resultCode=" + convert.Int32ToString(int32(resultCode)) + " sqlComit=" + sqlComit
		ctx.Log(msg)
		ctx.ErrorResult(msg)
		return
	} else {
		ctx.Log("commit success.")
	}
}

//export unpredictableSql
func unpredictableSql() {
	ctx := NewSqlSimContext()
	ctx.Log("init_contract [start]")
	sqlUnpredictable := "insert into student_gasm(id, name, age, id_card_no) VALUES ('34134','whang','1234'+Rand()*200,'1342551153')"
	ctx.Log(sqlUnpredictable)
	_, resultCode := ctx.ExecuteUpdate(sqlUnpredictable)
	if resultCode != SUCCESS {
		msg := "initContract error. resultCode=" + convert.Int32ToString(int32(resultCode)) + " sqlUnpredictable=" + sqlUnpredictable
		ctx.Log(msg)
		ctx.ErrorResult(msg)
		return
	} else {
		ctx.Log("detect table1 success.")
	}
}

//export autoIncrementSql
func autoIncrementSql() {
	ctx := NewSqlSimContext()
	ctx.Log("init_contract [start]")
	sqlIncrement := `create table whang2(
				id int(32) primary key auto_increment,
				username varchar(50)
							)`
	ctx.Log(sqlIncrement)
	_, resultCode := ctx.ExecuteDdl(sqlIncrement)
	if resultCode != SUCCESS {
		msg := "initContract error. resultCode=" + convert.Int32ToString(int32(resultCode)) + " sqlIncrement=" + sqlIncrement
		ctx.Log(msg)
		ctx.ErrorResult(msg)
		return
	} else {
		ctx.Log("creat state_infos success.")
		return
	}
}

//export createDb
func createDb() {
	ctx := NewSqlSimContext()
	ctx.Log("init_contract [start]")
	sqlCreate := "create database day1"
	ctx.Log(sqlCreate)
	_, resultCode := ctx.ExecuteDdl(sqlCreate)
	if resultCode != SUCCESS {
		msg := "initContract error. resultCode=" + convert.Int32ToString(int32(resultCode)) + " sqlCreate=" + sqlCreate
		ctx.Log(msg)
		ctx.ErrorResult(msg)
		return
	} else {
		ctx.Log("creat database success.")
		return
	}
}

//export createUser
func createUser() {
	ctx := NewSqlSimContext()
	ctx.Log("init_contract [start]")
	sqlCreateuser := "CREATE USER \"sss\"@\"host\" identified by\"123\""
	ctx.Log(sqlCreateuser)
	_, resultCode := ctx.ExecuteDdl(sqlCreateuser)
	if resultCode != SUCCESS {
		msg := "initContract error. resultCode=" + convert.Int32ToString(int32(resultCode)) + " sqlCreateuser=" + sqlCreateuser
		ctx.Log(msg)
		ctx.ErrorResult(msg)
		return
	} else {
		ctx.Log("creat database success.")
		return
	}
}

func main() {

}
