package main

//
//import (
//	"strings"
//)
//
//// 安装合约时会执行此方法，必须
////export init_contract
//func initContract() {
//}
//// 升级合约时会执行此方法，必须
////export upgrade
//func upgrade() {
//}
////export bind_user
//func bindUser() {
//	appId, _ := ArgString("app_id")
//	pubkey, _ := ArgString("pubkey")
//	signature, _ := ArgString("signature")
//	blockNum, _ := ArgString("block_num")
//	txId, _ := ArgString("tx_id")
//	if isBlank(appId, pubkey, signature, blockNum, txId) {
//		ErrorResult("parameter err")
//		logMessage("appId: " + appId + ", pubkey: " + pubkey + ", signature: " + signature +
//			", blockNum: " + blockNum + ", txId: " + txId)
//		return
//	}
//	rawStr := strings.Join([]string{appId, pubkey}, ",")
//	ok, err := verify(pubkey, signature, rawStr)
//	if err != nil {
//		ErrorResult("verify date failed")
//		return
//	}
//	if !ok {
//		ErrorResult("check signature failed")
//		return
//	}
//
//	items := []*EasyCodecItem {
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "app_id",
//
//			ValueType: EasyValueType_STRING,
//			Value: appId,
//		},
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "pubkey",
//
//			ValueType: EasyValueType_STRING,
//			Value: pubkey,
//		},
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "signature",
//
//			ValueType: EasyValueType_STRING,
//			Value: signature,
//		},
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "block_num",
//
//			ValueType: EasyValueType_STRING,
//			Value: blockNum,
//		},
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "tx_id",
//
//			ValueType: EasyValueType_STRING,
//			Value: txId,
//		},
//	}
//	bytes := EasyMarshal(items)
//	logMessage("save user info, key: evidence, field(appId): " + appId + ", bytes: " + string(bytes))
//	PutStateByte("evidence", appId, bytes)
//}
//
////export upgrade_user
//func upgradeUser() {
//	appId, _ := ArgString("app_id")
//	pubkey, _ := ArgString("pubkey")
//	signature, _ := ArgString("signature")
//	blockNum, _ := ArgString("block_num")
//	txId, _ := ArgString("tx_id")
//	if isBlank(appId, pubkey, signature, blockNum, txId) {
//		ErrorResult("parameter err")
//		logMessage("appId: " + appId + ", pubkey: " + pubkey + ", signature: " + signature +
//			", blockNum: " + blockNum + ", txId: " + txId)
//		return
//	}
//	rawStr := strings.Join([]string{appId, pubkey}, ",")
//	ok, err := verify(pubkey, signature, rawStr)
//	if err != nil {
//		ErrorResult("verify date failed")
//		return
//	}
//	if !ok {
//		ErrorResult("check signature failed")
//		return
//	}
//
//	items := []*EasyCodecItem {
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "app_id",
//
//			ValueType: EasyValueType_STRING,
//			Value: appId,
//		},
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "pubkey",
//
//			ValueType: EasyValueType_STRING,
//			Value: pubkey,
//		},
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "signature",
//
//			ValueType: EasyValueType_STRING,
//			Value: signature,
//		},
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "block_num",
//
//			ValueType: EasyValueType_STRING,
//			Value: blockNum,
//		},
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "tx_id",
//
//			ValueType: EasyValueType_STRING,
//			Value: txId,
//		},
//	}
//	bytes := EasyMarshal(items)
//	logMessage("save user info, key: evidence, field(appId): " + appId + ", bytes: " + string(bytes))
//	PutStateByte("evidence", appId, bytes)
//}
//
////export get_user
//func getUser() {
//	appId, _ := ArgString("app_id")
//	if len(appId) == 0 {
//		ErrorResult("appId should not be nil")
//		return
//	}
//	userBytes, rc := GetStateByte("evidence", appId)
//	if rc == ERROR {
//		ErrorResult("get user data failed")
//		return
//	}
//	if len(userBytes) == 0 {
//		ErrorResult("user data nil")
//		return
//	}
//
//	SuccessResultByte(userBytes)
//}
//
////export add_evidence
//func addEvidence() {
//	appId, _ := ArgString("app_id")
//	eviId, _ := ArgString("evi_id")
//	eviHash, _ := ArgString("evi_hash")
//	eviExt, _ := ArgString("evi_ext")
//	eviTimestamp, _ := ArgString("evi_timestamp")
//	eviTime, _ := ArgString("evi_time")
//	eviSignature, _ := ArgString("evi_signature")
//	blockNum, _ := ArgString("block_num")
//	txId, _ := ArgString("tx_id")
//	if isBlank(appId, eviId, eviHash, eviTimestamp, eviTime, eviSignature, blockNum, txId) {
//		ErrorResult("parameter invalid")
//		logMessage("appId: " + appId + ", eviId: " + eviId + ", eviHash: " + eviHash +
//			", eviExt: " + eviExt + ", eviTimestamp: " + eviTimestamp + ", eviTime: " + eviTime +", eviSignature: " +
//			eviSignature)
//		return
//	}
//	//existEd, rc := GetState("evidence", eviId)
//	//if (rc == ERROR || !isBlank(existEd)) {
//	//	ErrorResult("evidence is already exist")
//	//	return
//	//}
//
//	//verify signature
//	//uiBytes, rc := GetStateByte("evidence", appId)
//	//if rc == ERROR {
//	//	ErrorResult("can't found user")
//	//	return
//	//}
//	//if len(uiBytes) == 0 {
//	//	ErrorResult("user info empty")
//	//	return
//	//}
//	//userItems := EasyUnmarshal(uiBytes)
//	//if userItems == nil || len(userItems) == 0 {
//	//	logMessage("EasyUnmarshal user info error, user bytes: " + string(uiBytes))
//	//	ErrorResult("user info error")
//	//}
//	//tmpUi := EasyCodecItemToParamsMap(userItems)
//	//if tmpUi == nil || len(tmpUi) == 0 {
//	//	logMessage("EasyCodecItemToParamsMap error, user bytes: " + string(uiBytes))
//	//	ErrorResult("user info error")
//	//}
//	//if isBlank(tmpUi["pubkey"]) {
//	//	logMessage("user pubkey error, user bytes: " + string(uiBytes))
//	//	ErrorResult("user info error")
//	//	return
//	//}
//	//rawStr := strings.Join([]string{appId, eviHash, eviExt, eviId}, ",")
//	//ok, err := verify(tmpUi["pubkey"], eviSignature, rawStr)
//	//if err != nil {
//	//	ErrorResult("verify signature failed")
//	//	return
//	//}
//	//if !ok {
//	//	ErrorResult("check signature error")
//	//	return
//	//}
//	// 序列化为json bytes
//	items := []*EasyCodecItem{
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "app_id",
//
//			ValueType: EasyValueType_STRING,
//			Value: appId,
//		},
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "evi_id",
//
//			ValueType: EasyValueType_STRING,
//			Value: eviId,
//		},
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "evi_hash",
//
//			ValueType: EasyValueType_STRING,
//			Value: eviHash,
//		},
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "evi_ext",
//
//			ValueType: EasyValueType_STRING,
//			Value: eviExt,
//		},
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "evi_timestamp",
//
//			ValueType: EasyValueType_STRING,
//			Value: eviTimestamp,
//		},
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "evi_time",
//
//			ValueType: EasyValueType_STRING,
//			Value: eviTime,
//		},
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "evi_signature",
//
//			ValueType: EasyValueType_STRING,
//			Value: eviSignature,
//		},
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "block_num",
//
//			ValueType: EasyValueType_STRING,
//			Value: blockNum,
//		},
//		{
//			KeyType: EasyKeyType_USER,
//			Key: "tx_id",
//
//			ValueType: EasyValueType_STRING,
//			Value: txId,
//		},
//	}
//	bytes := EasyMarshal(items)
//	// 存储数据
//	logMessage("save evidence data, key: evidence, field: " + eviId + ", bytes: " + string(bytes))
//	PutStateByte("evidence", eviId, bytes)
//	// 返回结果
//	SuccessResult("ok")
//}
//
////export get_evi_data
//func getEviData() {
//	eviId, _ := ArgString("evi_id")
//	if len(eviId) == 0 {
//		ErrorResult("eviId should not be nil")
//	}
//	eviData, rc := GetStateByte("evidence", eviId)
//	if rc == ERROR {
//		ErrorResult("get user data failed")
//		return
//	}
//	if len(eviData) == 0 {
//		ErrorResult("user data nil")
//		return
//	}
//
//	SuccessResultByte(eviData)
//}
//
//func isBlank(arg ...string) bool {
//	for i := 0; i < len(arg); i++ {
//		if len(arg[i]) == 0 {
//			return true
//		}
//	}
//	return false
//}
//
//func verify(pkStr, sigStr, msgString string) (ok bool, err error) {
//	return true, nil
//	//	pkBytes, err := hex.DecodeString(pkStr)
//	//	if err != nil {
//	//		return false, fmt.Errorf("incorrect pk format, %s", err)
//	//	}
//	//	sigBytes, err := hex.DecodeString(sigStr)
//	//	if err != nil {
//	//		return false, fmt.Errorf("incorrect signature format, %s", err)
//	//	}
//	//	msgBytes := []byte(msgString)
//	//	x, y := elliptic.Unmarshal(curve, pkBytes)
//	//	pubilcKey := &sm2.PublicKey{curve, x, y}
//	//	sig := new(Signature)
//	//	_, error := asn1.Unmarshal(sigBytes, sig)
//	//	if error != nil {
//	//		return false, error
//	//	}
//	//
//	//	result := sm2.Verify(pubilcKey, msgBytes, sig.R, sig.S)
//	//
//	//	return result, nil
//}
//
//func main() {
//}
